This README file may help to get a basic overview of the project

## Requirements

info: no Operations - "n0p's" intend to be solved solely by design<br/>
for example "Kassieren" uses the function removeAllFromCart() which already exists

| Task           | Tests | Funcs | Fronts | Finished | 
|----------------|-------|-------|--------|----------|
| Select Category | nOp | nOp | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Select default Category | nOp | nOp | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Select random Category on no default | <ul><li> [ ] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Sort Productlist Asc&Desc | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Add Articles to cart | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Adding same articles raises quant in cart += 1 | nOp | nOp | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Remove an article from cart | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Remove all articles from cart | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Show cart summary - quant & price | nOp | nOp | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| "Kassieren" empties cart | nOp | nOp | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>
| Keep cart & default-cat on page reload | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [x] </li></ul> | <ul><li> [ ] </li></ul>



## TDD -> BDD

```
   actions.tests.js
        sort                ->  Productview ASC & DESC
                                could be used by:  ProductView, CategoryView and Cart
        save                ->  Cart and DefaultCategory must not get lost on page reload
                                Set shown Category to random if no saved state is found

   cart.tests.js
        addToCart           ->  add items to cart by clicking on a product
                                by clicking again, the quantity in the Cart += 1
        removeFromCart      ->  remove an article from the cart -> <DeleteIcon /> on ProductView
                                remove all articles from Cart   -> <DeleteIcon /> for all products on Cart
```
