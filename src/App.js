import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CategoryView from './components/CategoryView.js';
import ProductView from './components/ProductView.js';
import Cart from './components/Cart.js';
import { initConstants } from './constants.js'; 
import { sortByKey } from './actions/misc.actions.js';
import { addToCart, removeFromCart } from './actions/cart.actions.js';

class App extends Component {

    constructor(props) {
        super(props);

        let savedData = (JSON.parse(localStorage.getItem('state')) || initConstants);
        let id = 0;

        if( savedData.defaultCategory === -1 )
            id = Math.floor(Math.random() * Math.floor(3));
        else
            id = savedData.defaultCategory;

        this.state = {
            categories: savedData.categories,
            selectedCategory: savedData.categories[id],
            defaultCategory: savedData.defaultCategory,
            cart: savedData.cart,
        };
    }

    saveState = () => {
        localStorage.setItem('state', JSON.stringify(this.state));
    }

    handleClickCategory = (e) => {
        e.preventDefault();
        let tmp = this.state.categories[e.target.id];
        this.setState({
            selectedCategory: tmp,
        });
    }
    handleClickSetDefaultCategory = (e) => {
        e.preventDefault();
        let tmp = this.state.categories.indexOf(this.state.selectedCategory);
        this.setState({
            defaultCategory: tmp,
        }, this.saveState);
    }
    handleClickProduct = (e) => {
        let tmp = this.state.cart;
        addToCart(tmp, this.state.selectedCategory.products[e.target.id]);
        //the following sort is to have a defined order in cart
        tmp.sort((a, b) => a.title > b.title);
        this.setState({
            cart: tmp,
        }, this.saveState);
    }
    handleClickRemoveProduct = (e) => {
        let tmp = this.state.cart;
        removeFromCart(tmp, e.target.id);
        this.setState({
            cart: tmp,
        }, this.saveState);
    }
    handleClickSortProducts = (e) => {
        let category = this.state.selectedCategory;
        sortByKey(category.products, e.target.id);
        this.setState({
            selectedCategory: category,
        }, this.saveState);
    }

    render() {
        let categoryList = this.state.categories.map((category, i) => (
            <CategoryView key={"categoryView_"+i} title={category.title} id={i} onClick={this.handleClickCategory}/>
        ));

        let productList = this.state.selectedCategory.products.map((product, i) => (
            <ProductView key={"productView_"+i} data={product} id={i} onClick={this.handleClickProduct}/>
        ));

        let setDefaultButton;
        if( JSON.stringify(this.state.selectedCategory) === JSON.stringify(this.state.categories[this.state.defaultCategory]) ){
            setDefaultButton = <button className="SetDefaultButton" disabled> Is default </button>
        }else{
            setDefaultButton = <button className="SetDefaultButton" onClick={this.handleClickSetDefaultCategory}> Make default </button>
        }

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>Demo App</p>
                    <div className="Menu">
                        {categoryList}
                    </div>
                </header>
                <div>
                    {setDefaultButton}
                </div>
                <div className="MainFrame">
                    <table> 
                        <tbody>
                            <tr>
                                <td>
                                    <Cart data={this.state.cart} remove={this.handleClickRemoveProduct}/>
                                </td>
                                <td className="ProductList">
                                    <button onClick={this.handleClickSortProducts} id="title">Sort by title</button>
                                    <button onClick={this.handleClickSortProducts} id="price">Sort by price</button><br />
                                    {productList}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
export default App;
