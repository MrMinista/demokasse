import React from 'react'
import renderer from 'react-test-renderer'
import App from '../App';
import { initConstants } from '../constants.js';

describe('App snapshot test', () => {
    it('snapshot test with default category', () => {
        //Setting defaultCategory to 0 here because -1(default) renders a random category and snapshot fails.
        initConstants.defaultCategory = 0;
        const tree = renderer.create(<App />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
