import { addToCart, removeFromCart } from '../actions/cart.actions.js';
import { initConstants } from '../constants.js';

describe('Cart tests', () => {

    var cart = initConstants.cart;
    it('adds a product to cart', () => {
        addToCart(cart, initConstants.categories[0].products[0]);
        expect(cart[0].title ).toEqual('Pullover');
    });
    it('add a product that already exists in cart', () => {
        addToCart(cart, initConstants.categories[2].products[2]);
        expect(cart.length === 2).toEqual(true);
        expect(cart[cart.length-1].title).toEqual("Notebook");
    });
    it('remove a product from cart', () => {
        addToCart(cart, initConstants.categories[0].products[2]);
        expect(cart[1].title).toEqual('Notebook');
        var tmp = cart.length;
        removeFromCart(cart, 0);
        expect(cart[1].title).toEqual('Jeans');
        expect(cart.length).toEqual(tmp-1);
    });

    it('remove all products from cart', () => {
        removeFromCart(cart);
        expect(cart.length).toEqual(0);
    });

});
