import { sortByKey } from '../actions/misc.actions.js';
import { initConstants } from '../constants.js';

describe('Testing sort ASC & DESC by title & price' , () => {

    let categories = initConstants.categories;

    it('sort ASC by title', () => {
        sortByKey(categories[1].products, "title");
        expect(categories[1].products[0].title).toEqual("Apfel");
    });
    it('sort DESC by title on second use', () => {
        sortByKey(categories[1].products, "title");
        expect(categories[1].products[0].title).toEqual("Paprika");
    });
    it('sort back to ASC by title on third use', () => {
        sortByKey(categories[1].products, "title");
        expect(categories[1].products[0].title).toEqual("Apfel");
    });
    it('sort by price ASC', () => {
        sortByKey(categories[1].products, "price");
        expect(categories[1].products[0].title).toEqual("Brot");
    });
    it('sort by price DESC on additional use', () => {
        sortByKey(categories[1].products, "price");
        expect(categories[1].products[0].title).toEqual("Paprika");
    });

});


describe('Testing save and delete to/from local storage' , () => {

    localStorage.setItem('state', JSON.stringify("notyetset"));
    let tmp = JSON.parse(localStorage.getItem('state'));

    expect(tmp).toEqual("notyetset");
    expect(tmp === 'somethingelse').toEqual(false);

});

