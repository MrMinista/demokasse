

export function addToCart(cart, product){
    cart.push(product); 
    return cart; 
}

export function removeFromCart(cart, index){
    // '!===' not used here because this way its possible to leave index also 'undefined'
    index >= 0 ? 
        cart.splice(index, 1) : cart.splice(0, cart.length);
    return cart; 
}
