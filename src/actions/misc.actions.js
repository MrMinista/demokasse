
export function sortByKey(array, key){
    let tmparray = array.slice();
    return JSON.stringify(tmparray) === JSON.stringify(array.sort((a,b) => a[key] > b[key])) ?
        array.reverse() : array;
}
