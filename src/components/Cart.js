import React, {Component} from 'react';

class Cart extends Component{
    render(){
        let productsView = [];
        let productsInCart = [];
        let sum = 0;
        this.props.data.forEach((product, index) => {
            if(!productsInCart.includes(product.title)){
                productsInCart.push(product.title);
                let count = this.props.data.filter(function(tmp){return tmp.title === product.title}).length;
                productsView.push(<tr key={"cartRow_"+index}>
                    <td>{product.title}</td> 
                    <td>{"x"+count+" ="}</td> 
                    <td className="Numbers">{(product.price * count).toFixed(2)} €</td>
                    <td><button onClick={this.props.remove} id={index}>X</button></td></tr>
                );
            }
            sum += product.price;
        });
        productsView.push(<tr key="cart_hline"><td>-----</td><td>-----</td><td>-----</td></tr>);
        productsView.push(<tr key="cart_sum"><td>{this.props.data.length} products</td><td>=</td><td>{sum.toFixed(2)} €</td></tr>);

        return(
            <div className="Cart">
                <p>Shopping Cart</p>
                <table>
                    <tbody>
                        {productsView}
                    </tbody>
                </table>
                <button style={{width: '100%'}} onClick={this.props.remove} id={-1}>Kassieren</button>
            </div> 
        );
    }
}
export default Cart;
