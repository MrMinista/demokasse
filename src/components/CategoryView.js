import React from 'react';

const CategoryView = ({title, onClick, id}) => {
        return(
            <div className="CategoryView" id={id} onClick={onClick}>
                {title} 
            </div> 
        );
};
export default CategoryView;

