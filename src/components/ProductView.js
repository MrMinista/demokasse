import React, {Component} from 'react';

const ProductView = ({data, onClick, id}) => {
        return(
            <div className="ProductView">
                <div>
                    {data.title} <br/>
                    {data.price+" €"}
                </div>
                <button id={id} onClick={onClick}>
                    Add to cart
                </button>
            </div> 
        );
}
export default ProductView;
