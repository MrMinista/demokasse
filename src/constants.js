
export const initConstants = {
    categories: [
        {title: "Textil", products: [
            { title: "Pullover", price: 20 },
            { title: "T-Shirt", price: 8.5 },
            { title: "Jeans", price: 10 },
        ]},
        {title: "Nahrungsmittel", products: [
            { title: "Paprika", price: 10 },
            { title: "Eis am Stiel", price: 4 },
            { title: "Apfel", price: 2.5 },
            { title: "Kaese", price: 3 },
            { title: "Brot", price: 2 },
        ]},
        {title: "Office", products: [
            { title: "Druckpapier", price: 20 },
            { title: "Kugelschreiber", price: 5.5 },
            { title: "Notebook", price: 15 },
            { title: "USB-Ventilator", price: 10 },
        ]},
    ],
    defaultCategory: -1,
    cart: [],

};
